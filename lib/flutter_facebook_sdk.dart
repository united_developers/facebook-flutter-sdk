import 'dart:async';

import 'package:flutter/services.dart';

class FlutterFacebookSdk {
  static const MethodChannel _channel =
      const MethodChannel('flutter_facebook_sdk');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> get logEvent async {
    final String result = await _channel.invokeMethod('logEvent');
    return result;
  }

  static Future<void> logAddedToCartEvent(String contentData,
      String contentId, String contentType, double price) async {
    var params = <String, dynamic>{
      "EVENT_PARAM_CONTENT": contentData,
      "EVENT_PARAM_CONTENT_ID": contentId,
      "EVENT_PARAM_CONTENT_TYPE": contentType,
      "price": price
    };
    final String result = await _channel.invokeMethod('logAddedToCartEvent',params);
    return result;
  }

  static Future<void> logPurchase(double totalPrice) async {
    var params = <String, dynamic>{
      "totalPrice": totalPrice
    };
    final String result = await _channel.invokeMethod('logPurchase',params);
    return result;
  }



}
