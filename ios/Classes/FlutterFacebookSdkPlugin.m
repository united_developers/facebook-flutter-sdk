#import "FlutterFacebookSdkPlugin.h"
#import <flutter_facebook_sdk/flutter_facebook_sdk-Swift.h>

@implementation FlutterFacebookSdkPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterFacebookSdkPlugin registerWithRegistrar:registrar];
}
@end
