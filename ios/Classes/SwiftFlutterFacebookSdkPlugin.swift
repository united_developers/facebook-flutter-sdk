import Flutter
import UIKit
import FBSDKCoreKit

public class SwiftFlutterFacebookSdkPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flutter_facebook_sdk", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterFacebookSdkPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        guard let arguments = call.arguments as? [String: Any] else {return}
        switch call.method {
        case "logAddedToCartEvent":
            let params: [String: Any] = [
                AppEvents.ParameterName.contentID.rawValue : arguments["EVENT_PARAM_CONTENT_ID"] ?? "",
                AppEvents.ParameterName.contentType.rawValue : arguments["EVENT_PARAM_CONTENT_TYPE"] ?? "",
                AppEvents.ParameterName.currency.rawValue :  "EUR"
            ]
            AppEvents.logEvent(AppEvents.Name.addedToCart, valueToSum: arguments["price"] as? Double ?? 0, parameters: params)
            break
        case "logPurchase":
            guard let arguments = call.arguments as? [String: Any] else {return}
            AppEvents.logPurchase(arguments["totalPrice"] as? Double ?? 0, currency: "EUR")
            break
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
}
