package dev.udev.flutter_facebook_sdk

import android.os.Bundle
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsConstants
import com.facebook.appevents.AppEventsLogger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import java.math.BigDecimal
import java.util.Currency;


class FlutterFacebookSdkPlugin : MethodCallHandler {

    private val logger = AppEventsLogger.newLogger(FacebookSdk.getApplicationContext())


    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "flutter_facebook_sdk")
            channel.setMethodCallHandler(FlutterFacebookSdkPlugin())
        }
    }

    override fun onMethodCall(call: MethodCall, result: Result) = when {

        call.method == "logAddedToCartEvent" -> {

            val contentData = call.argument("EVENT_PARAM_CONTENT") as String?
            val contentId = call.argument("EVENT_PARAM_CONTENT_ID") as String?
            val contentType = call.argument("EVENT_PARAM_CONTENT_TYPE") as String?
            val currency = "EUR"
            val price =  call.argument("price") as Double?
            val params = Bundle()
            params.putString(AppEventsConstants.EVENT_PARAM_CONTENT, contentData)
            params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId)
            params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType)
            params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency)
            logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, (price ?: 0) as Double, params)
            result.success("Android logEvent EVENT_NAME_ADDED_TO_CART")
        }
        call.method == "logPurchase" -> {
            val totalPrice =  call.argument("totalPrice") as Double?
            logger.logPurchase(totalPrice?.toBigDecimal(), Currency.getInstance("EUR"), Bundle())
            result.success("Android logPurchase")
        }
        else -> result.notImplemented()
    }
}
